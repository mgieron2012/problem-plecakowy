def heuristic_solve(weights, values, max_weight):
    number_of_elements = len(weights)
    real_values = []

    for idx in range(number_of_elements):
        real_values.append((values[idx] / weights[idx], idx))

    sorted_real_values = sorted(real_values, reverse=True)
    _, indexes_of_sorted_real_values = zip(*sorted_real_values)

    number_of_elements_in_bagpack = 0
    current_weight = 0
    
    while current_weight <= max_weight and number_of_elements_in_bagpack <= number_of_elements:
        current_weight += weights[indexes_of_sorted_real_values[number_of_elements_in_bagpack]]
        number_of_elements_in_bagpack += 1
    
    return [(i+1, values[i], weights[i]) for i in indexes_of_sorted_real_values[:number_of_elements_in_bagpack-1]]


def solve(weights, values, max_weight):
    number_of_elements = len(weights)
    best_combination = 0
    result_value = 0

    for combination in range(1, 2**number_of_elements - 1):
        current_value = 0
        current_weight = 0
        for idx in range(number_of_elements):
            if (combination >> idx) & 1:
                current_weight += weights[idx]
                current_value += values[idx]
        if current_weight <= max_weight and current_value > result_value:
            result_value = current_value
            best_combination = combination
    
    result_elements = []
    for idx in range(number_of_elements):
        if (best_combination >> idx) & 1:
            result_elements.append((idx+1, values[idx], weights[idx]))
    return result_elements

if __name__ == "__main__":
    print(heuristic_solve([8, 3, 5, 2], [16, 8, 9, 6], 9))
    print(solve([8, 3, 5, 2], [16, 8, 9, 6], 9))

    print(solve([8, 3, 5, 2, 6, 4, 3, 12, 12, 12, 1, 12, 12, 43, 23, 65, 22, 3, 1, 1, 2, 2, 3, 6], [16, 8, 9, 6, 1, 2, 2, 1, 1, 2, 3, 3, 3, 4, 5, 10, 4, 3, 2, 1, 5, 4, 10, 3], 9))
    print(heuristic_solve([8, 3, 5, 2, 6, 4, 3, 12, 12, 12, 1, 12, 12, 43, 23, 65, 22, 3, 1, 1, 2, 2, 3, 6], [16, 8, 9, 6, 1, 2, 2, 1, 1, 2, 3, 3, 3, 4, 5, 10, 4, 3, 2, 1, 5, 4, 10, 3], 9))
