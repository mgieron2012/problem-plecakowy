# Problem plecakowy


## Zadanie
Znaleźć rozwiązanie optymalne przez przegląd wyczerpujący.
Rozwiązać problem przy użyciu heurystyki: do plecaka pakujemy przedmioty według kolejności wynikającej ze stosunku p/w. Uwaga: heurystyka to nie funkcja heurystyczna. Nie używamy tu jeszcze funkcji heurystycznej i algorytmu A*.

## Pytania

Jakie rozwiązania i jaką wartość funkcji oceny uzyskano? Czy uzyskano takie same rozwiązania?
Jak dużą instancję problemu (liczba przedmiotów) da się rozwiązać w około minutę metodą zachłanną?
Jak bardzo wydłuży obliczenia dodanie jeszcze jednego przedmiotu?
Jakie wnioski można wyciągnąć na podstawie wyników tego ćwiczenia?

## Odpowiedzi

Uzyskane rozwiązania:
(index, value, weight)
(4, 6, 2), (2, 8, 3) 14 dla heurystyki
(2, 8, 3), (3, 9, 5) 17 dla przeglądu wyczerpującego

Metodą zachłanną w około minutę można rozwiązać problem zawierający 24 przedmioty.

Dodanie przedmiotu wydłuży dwukrotnie czas algorytmu dla przeglądu wyczerpującego.
W przypadku heurystyki zależy to od funkcji sortowania.

Metoda heurystyczna nie znajduje zawsze najlepszego wyniku (choć zazwyczaj bliski), ale działa dużo szybciej od przeglądu wyczerpującego.
